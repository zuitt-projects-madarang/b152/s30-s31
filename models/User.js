const mongoose = require("mongoose")

//SCHEMA
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

//MODEL
module.exports = mongoose.model("User",userSchema)