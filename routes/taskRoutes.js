const express = require("express")
//Router() is a method from express that allows us the access to our HTTP method routes.
//Router() will act as a middleware and our routing system.
const router = express.Router()



//ExpressJS routes should not handle the business logic of our application. Routes are only meant to route client request by their endpoint and method. Handling the requests and responses should not be done in the routes. Instead, we should have separate functions to handle our request and response. These separate functions that handle request and responses and the business logic of our application are controllers.
const taskControllers = require('../controllers/taskControllers')

// console.log(taskControllers);

//create task route
//endpoint: /tasks
router.post('/',taskControllers.createTaskController)

//get all the task documents and send it to the client:
//endpoint: /tasks/
router.get('/',taskControllers.getAllTaskController)

//get a single task's details
//URL: http://localhost:4000/tasks/getSingleTask/61ee3a18ec62e40a67851c71
router.get('/getSingleTask/:id',taskControllers.getSingleTaskController)

//update a single task's status
//URL: http://localhost:4000/tasks/updateTaskStatus/61ee3a18ec62e40a67851c71
router.put('/updateTaskStatus/:id',taskControllers.updateTaskStatusController)

module.exports = router