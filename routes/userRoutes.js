const express = require("express")
const router = express.Router()

//import userControllers
const userControllers = require('../controllers/userControllers')


router.post('/',userControllers.createUserController)


router.get('/',userControllers.getAllUserController)

router.put('/updateUserUsername/:id',userControllers.updateUserUsernameController)

router.get('/getSingleUser/:id',userControllers.getSingleUserController)

module.exports = router