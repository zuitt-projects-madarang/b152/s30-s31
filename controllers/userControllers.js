//import the User model:
const User = require('../models/User')

module.exports.createUserController = (req,res) => {
	console.log(req.body)

	//We should now allow users to have the same username. Thus, we will add a validation.
	//Check if there is a document who shares the same username as our input:
	//Model.findOne() is a mongoose method which is similar to db.collection.findOne() in MongoDB
	//However, with Model.findOne(), we can process the result via our api:
	//findOne document which username property matches with the username provided in the request body.
	//if findOne() cannot find a document that matches your criteria, it will return null.
	User.findOne({username: req.body.username})
	.then(result =>{
		//console.log(result); 
		//console log the result of findOne(). If findOne() cannot find a matching document, it will return null.
		//If findOne() is able to find a document that matched our criteria, it will return the document.
		if(result !== null && result.username === req.body.username){

			//return keyword is added here to end the process and the code block.
			//the execution of the statement will end at res.send()
			return res.send("Duplicate User Found");
		} else {
			let newUser = new User({

				username: req.body.username,
				password: req.body.password

			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error))
		}
	})
	.catch(error => res.send(error))
}

module.exports.getAllUserController = (req,res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateUserUsernameController = (req,res) => {
	//console.log(req.params.id)
	//console.log(req.body)

	let updates = {
		username: req.body.username
	}

	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))
}

module.exports.getSingleUserController = (req,res) => {
	//console.log(req.params.id)
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}