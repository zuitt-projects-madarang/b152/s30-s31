const express = require("express");
/*
	Mongoose is a package which is used as an ODM, Object Document Mapper. It allows us to translate our JS objects into database documents for MongoDB. It allows connection and easier manipulation of our documents in mongoDB.
*/
const mongoose = require("mongoose");
/*All packages to be used should be required at the top of the file to avoid tampering or errors.*/

/*
	run npm start if the server is ready for hosting.
	npm run dev if the server is under development and needs to used nodemon.
*/
const app = express();
const port = 4000

//mongoose connection
/*
	mongoose.connect is the method to connect your api to your mongoDB via the use of mongooze. It has 2 arguments. First, is the connection string to connect our api to our mongoDB atlas. Second, is an object used to add information between mongoose and mongoDB.

	replace/change <password> in the connection string to your db password

	replace/change myFirstDatabase to task152

	MongoDB upon connection and creating our first documents will create the task152 database for us.
*/
mongoose.connect("mongodb+srv://aidz113:Hesoyam69420@cluster0.tuthz.mongodb.net/task152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

//We will create notifications if the connection to the db is a success or failed.
let db = mongoose.connection;
//We add this so that when the db has an error, we will show the connection error in both the terminal and in the browser for our client.
db.on('error',console.error.bind(console, "connection error."))
//Once the connection is open and successful, we will output a message in the terminal/gitbash.
db.once('open',()=>console.log("Connected to MongoDB"))

//Middleware - are methods, functions that acts and adds features to our application.
//express.json() - handle the body of the request. It handles the JSON data from our client.
app.use(express.json());

//Routes
//import taskRoutes
const taskRoutes = require('./routes/taskRoutes')
//Our server will use a middleware to group all task routes under /tasks
//Meaning to say, all the endpoints in taskRoutes file will start with /tasks
app.use('/tasks',taskRoutes)

//import userRoutes
const userRoutes = require('./routes/userRoutes')
//group userRoutes under /users
app.use('/users',userRoutes)



app.listen(port,()=>console.log(`Server is running at port ${port}`));